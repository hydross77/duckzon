<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(AnnonceRepository $annonceRepo): Response
    {
        // affiche les 3 dernières annonces
        $annonces3 = $annonceRepo->findLatestNotSold();
        return $this->render('home/index.html.twig', [
            'annonces3' => $annonces3
        ]);
    }

}
