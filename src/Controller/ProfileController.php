<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/profile')]
class ProfileController extends AbstractController
{

    #[Route('/annonce', name: 'app_profile')]
    public function index(User $user): Response
    {

        $annonces = $user->getAnnonces();

        return $this->render('profile/index.html.twig', [
            'annonces' => $annonces, // récupère toutes les annonces de l'utilisateur
        ]);
    }

}
