<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Entity\User;
use App\Form\AnnonceType;
use App\Repository\AnnonceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Require ROLE_ADMIN for all the actions of this controller
 */
#[IsGranted('ROLE_ADMIN')]
class AdminController extends AbstractController
{

    private $em;
    public function __construct(EntityManagerInterface $em, AnnonceRepository $annonceRepo){
        $this->em = $em;
        $this->annonceRepo = $annonceRepo;
    }

    public function adminDashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // or add an optional message - seen by developers
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
    }


    #[Route('/annonce', name: 'app_admin_annonce')]
    public function index(AnnonceRepository $annonceRepo): Response
    {
        $annonces = $annonceRepo->findAll();
        return $this->render('admin/annonce/view.html.twig', [
            'annonces' => $annonces
        ]);
    }

    #[Route('/annonce/{id}/edit', name: 'app_admin_annonce_edit')]
    public function edit(Annonce $annonce, Request $request, EntityManagerInterface $em, User $user): Response
    {

        if ($annonce->getUser() !== $user) {
            $this->addFlash('warning', 'Vous ne pouvez pas accéder à cette ressource');
            return $this->redirectToRoute('profile');
        }

        $form = $this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('app_admin_annonce');
        }


        return $this->render('admin/annonce/edit.html.twig', [
            'annonce' => $annonce,
            'form' => $form->createView()
        ]);
    }

    #[Route('annonce/{id}', name: 'app_admin_annonce_delete', methods: 'DELETE')]
    public function delete(Annonce $annonce, EntityManagerInterface $em, Request $request) : Response
    {
        if ($this->isCsrfTokenValid('delete' . $annonce->getId(), $request->get('_token'))) {
            $em->remove($annonce);
            $em->flush();

            $this->addFlash("message", "Annonce supprimé avec succès.");
            return $this->redirectToRoute("app_admin_annonce");
        }
    }
}
