<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Entity\AnnonceSearch;
use App\Entity\User;
use App\Form\AnnonceSearchType;
use App\Form\AnnonceType;
use App\Repository\AnnonceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class AnnonceController extends AbstractController
{

    private $em;
    public function __construct(EntityManagerInterface $em, AnnonceRepository $annonceRepo){
        $this->em = $em;
        $this->annonceRepo = $annonceRepo;
    }

    #[Route('/annonce/search', name: 'search_annonce')]
    public function search(Request $request, AnnonceRepository $annonceRepo, PaginatorInterface $paginator)
    {
        $annonceSearch = new AnnonceSearch();
        $form = $this->createForm(AnnonceSearchType::class, $annonceSearch);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            //$annonces = $annonceRepo->search($annonceSearch);

            $annonces = $paginator->paginate(
                $annonceRepo->search($annonceSearch),
                $request->query->getInt('page', 1),
                12
            );

            return $this->render('annonce/index.html.twig', [
                'annonces' => $annonces,
            ]);

        }

        return $this->render('annonce/_search-form.html.twig', [
            'form' => $form->createView()
        ]);
    }



    #[Route('/annonces', name: 'app_annonce')]
    public function index(Request $request, PaginatorInterface $paginator, AnnonceRepository $annonceRepo): Response
    {
        // rechercher une annonce par ID
//        $annonce = $annonceRepo->find(1);
//        dump($annonce);

       // recherche une annonce par champ
//      $annonce = $annonceRepo->findOneBy(['sold' => false]);
//      dump($annonce);

     // recherche toutes les annonces
//        $annonces = $annonceRepo->findAllNotSold();

        // les annonces paginé
        $annonces = $paginator->paginate(
            $annonceRepo->findAllNotSoldQuery(),
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('annonce/index.html.twig', [
            'annonces' => $annonces,
        ]);

    }

//    #[Route('/annonce/new', name: 'new_annonce')]
//    public function new(): \Symfony\Component\HttpFoundation\RedirectResponse
//    {
//        $annonce = new Annonce();
//        $annonce
//            ->setTitle('Ma collection de canard vivant')
//            ->setSlug('Vends Super Canard trouvé en boulangerie-pâtisserie')
//            ->setDescription('Vends car plus d\'utilité')
//            ->setPrice(10)
//            ->setStatus(Annonce::STATUS_BAD)
//            ->setSold(false)
//        ;
//
//        // On « persiste » l'entité
//        $this->em->persist($annonce);
//        // On envoie tout ce qui a été persisté avant en base de données
//        $this->em->flush();
//
//        $this->addFlash("message", "Annonce ajouté avec succès.");
//        return $this->redirectToRoute("app_home");
//    }

//    #[Route('/annonce/{id}', name: 'show_annonce', requirements: ["id"=>"\d+"])]
//    public function show(Annonce $annonce): Response
//    {
//        return $this->render('annonce/show.html.twig', [
//            'annonce' => $annonce,
//        ]);
//    }

    #[Route('/annonce/{slug}-{id}', name: 'show_annonce', requirements: ["slug" => "[a-z0-9\-]*", "id" => "\d+"])]
    public function showBySlug(Annonce $annonce, $slug): Response
    {
        //$annonce = $this->annonceRepository->find($id);

        return $this->render('annonce/show.html.twig', [
            'current_menu' => 'app_annonce',
            'annonce' => $annonce, // Symfony fait le find à notre place grâce à l'injection et l'id
        ]);

    }

    #[Route('annonce/new', name: 'new_annonce')]
    public function new(Request $request, EntityManagerInterface $em) : Response
    {
        $annonce = new Annonce();

        $form = $this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($annonce);
            $em->flush();
            $this->addFlash("message", "Annonce ajouté avec succès.");
            return $this->redirectToRoute("app_home");
        }

        return $this->render('annonce/new.html.twig', [
            'annonce' => $annonce,
            'form' => $form->createView()
        ]);
    }

    #[Route('/annonce/{id}/edit', name: 'app_annonce_edit')]
    public function edit(Annonce $annonce, Request $request, EntityManagerInterface $em, User $user): Response
    {

        if ($annonce->getUser() !== $user) {
            $this->addFlash('warning', 'Vous ne pouvez pas accéder à cette ressource');
            return $this->redirectToRoute('profile');
        }

        $form = $this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('app_annonce');
        }


        return $this->render('admin/annonce/edit.html.twig', [
            'annonce' => $annonce,
            'form' => $form->createView()
        ]);
    }

    #[Route('annonce/{id}', name: 'app_annonce_delete', methods: 'DELETE')]
    public function delete(Annonce $annonce, EntityManagerInterface $em, Request $request) : Response
    {
        if ($this->isCsrfTokenValid('delete' . $annonce->getId(), $request->get('_token'))) {
            $em->remove($annonce);
            $em->flush();

            $this->addFlash("message", "Annonce supprimé avec succès.");
            return $this->redirectToRoute("app_admin_annonce");
        }
    }

}
