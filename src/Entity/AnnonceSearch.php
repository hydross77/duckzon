<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class AnnonceSearch
{

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var ArrayCollection
     */
    private $tags;


    /**
     * @var int|null
     */
    private $status;

    /**
     * @var int|null
     */
    private $maxPrice;

    /**
     * @var \DateTimeInterface|null
     */
    private $createdAt;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title = null): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;
    }

    public function setMaxPrice(?int $maxPrice = null)
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status = null): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt = null): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of tags
     *
     * @return ArrayCollection
     */
    public function getTags(): ? ArrayCollection
    {
        return $this->tags;
    }

    /**
     * Set the value of tags
     *
     * @param ArrayCollection $tags
     * @return self
     */
    public function setTags(ArrayCollection $tags): static
    {
        $this->tags = $tags;
        return $this;
    }

}
